provider "aws" {
  region = "eu-central-1" # Change to your desired AWS region
}

resource "aws_instance" "example" {
  ami           = "ami-0766f68f0b06ab145" # Amazon Linux 2 AMI ID, replace with your desired AMI
  instance_type = "t2.micro"              # Change to your desired instance type

  tags = {
    Name = "ExampleInstance"
  }
}

output "instance_id" {
  value = aws_instance.example.id
}