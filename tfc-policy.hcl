# Allow tokens to query themselves
path "auth/token/lookup-self" {
  capabilities = ["read"]
}
# Allow tokens to renew themselves
path "auth/token/renew-self" {
    capabilities = ["update"]
}
# Allow tokens to revoke themselves
path "auth/token/revoke-self" {
    capabilities = ["update"]
}
# Allow tokens to create child tokens
path "auth/token/create" {
    capabilities = ["update"]
}
# Configure the actual secrets the token should have access to
path "secret/*" {
  capabilities = ["read"]
}
# Read from Azure secrets engine
path "azure/*" {
  capabilities = ["read"]
}
# Read from AWS Secrets Engine
path "aws/*" {
  capabilities = ["read"]
}
